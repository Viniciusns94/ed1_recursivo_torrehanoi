#include <stdio.h>
#include <stdlib.h>

void Hanoi (int n, char orig, char enter, char dest){
	if(n==1) 
		printf("Mova o disco 1 da pilha %c para %c\n", orig, dest);
	else{
		Hanoi(n-1,orig, dest, enter);
		printf("Mova o disco %d da pilha %c para %c\n",n, orig, dest);
		Hanoi(n-1, enter, orig, dest);
	}
}

void main(){
	Hanoi(5, 'A', 'B', 'C');
}